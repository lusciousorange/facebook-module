<?php
/**
 * Class TMv_FacebookAppPlugin
 */
class TMv_FacebookAppPlugin extends TSv_Plugin
{
	
	/**
	 * TMv_FacebookAppPlugin constructor.
	 * @param $plugin_model
	 */
	public function __construct($plugin_model)
	{
		parent::__construct($plugin_model);
		$this->setPluginTitle('Facebook App');
		$this->setPluginDescription('Installed the Facebook API tied to an app ID.');
		$this->setPluginIconName('fab fa-facebook');
		$this->enableVariableForNum(1, 'App ID','');
		$this->setDefaultInstallValue('enable_admin', 0);		
		

		
	}
	
	/**
	 * Renders the App Plugin
	 */
	public function render()
	{
		
		//if($this->pluginVariable(1) != '')
		//{
			$this->setIDAttribute('fb-root');
			$this->addJSLine('facebook_js_api', "
			window.fbAsyncInit = function() {
		        FB.init({
		          appId      : '".$this->pluginVariable(1)."',
		          xfbml      : true,
		          version    : 'v2.2'
		        });
		        $('#fb-root').trigger('facebook:init'); // trigger custom event
		      };

			(function(d, s, id) 
			{
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = '//connect.facebook.net/en_US/all.js#xfbml=1';
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));");
			

		//}
	}
	
}
?>