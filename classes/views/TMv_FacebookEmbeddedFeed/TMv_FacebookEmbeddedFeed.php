<?php
class TMv_FacebookEmbeddedFeed extends TCv_View
{
	use TMt_PagesContentView;
	
	protected $handle = '';
	protected $num_tweets, $hide_cover, $small_header, $height, $show_facepile;
	


	
	public function render()
	{
		if(!TC_website()->pluginInstalled('TMv_FacebookAppPlugin'))
		{
			$this->addText('Facebook App Plugin must be installed.');
		}



		if($this->handle != '')
		{
			$this->addClassJQueryFile();
			$this->addClassJQueryInit();

			$container = new TCv_View();
			$container->addClass('fb-page');
			$container->addDataValue('href', 'https://www.facebook.com/' . $this->handle );
			$container->addDataValue('tabs', 'timeline' );
			$container->addDataValue('hide-cover', $this->hide_cover ? 'true' : 'false' );
			$container->addDataValue('small-header', $this->small_header ? 'true' : 'false'  );
			$container->addDataValue('data-adapt-container-width', 'true' );
			$container->addDataValue('width', '200' );
			$container->addDataValue('height', $this->height );
			$container->addDataValue('show-facepile', $this->show_facepile ? 'true' : 'false');
			$this->attachView($container);

		}


	}



	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////


	/**
	 * Editor Form Items for the content form
	 * @return array
	 */
	public function pageContent_EditorFormItems()
	{
		$form_items = array();
	
		$field = new TCv_FormItem_TextField('handle', 'Facebook Handle ');
		$field->setHelpText('The handle that comes directly after facebook.com/handle/other-stuff/ ');
		$form_items[] = $field;

		$field = new TCv_FormItem_TextField('height', 'Height');
		$field->setHelpText('The height in pixels of the container.');
		$field->setDefaultValue(300);
		$form_items[] = $field;

		$field = new TCv_FormItem_Select('hide_cover', 'Cover Photo');
		$field->setHelpText('Indicate if the cover photo should be hidden');
		$field->addOption('0', 'Show the cover photo');
		$field->addOption('1', 'Hide the cover photo');
		$form_items[] = $field;


		$field = new TCv_FormItem_Select('small_header', 'Header Size');
		$field->setHelpText('Indicate if the small header should be used');
		$field->addOption('0', 'Large Header');
		$field->addOption('1', 'Small Header');
		$form_items[] = $field;

		$field = new TCv_FormItem_Select('show_facepile', 'Faces');
		$field->setHelpText('Indicate if the faces should appear');
		$field->addOption('0', 'No Faces');
		$field->addOption('1', 'Show Faces');
		$form_items[] = $field;


		return $form_items;
	}


	public static function pageContent_ViewTitle() { return 'Facebook User Timeline'; }
	public static function pageContent_IsAddable()
	{
		// Depends on if the Facebook plugin is installed
		return TC_website()->pluginInstalled('TMv_FacebookAppPlugin');
	}
	public static function pageContent_ViewDescription()
	{
		return 'An embedded timeline for a Facebook Page.';
	}

	public static function pageContent_ShowPreviewInBuilder()
	{
		return false;
	}
	
}

?>