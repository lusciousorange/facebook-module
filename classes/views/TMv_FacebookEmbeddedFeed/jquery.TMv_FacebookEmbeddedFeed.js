(function ($) 
{
// INIT
$.fn.TMv_FacebookEmbeddedFeed = function(options)
{
	var settings = $.extend(
	{
		timeout		:	null
	}, options || {});


	function resizeFacebook()
	{
		var $page = $('.fb-page');
		var new_width = $page.parent('div').width();
		$page.attr('data-width', new_width);
		$('.fb-page span').remove();
		FB.XFBML.parse();


	}

	return this.each(function()
	{ 
		$(window).resize( function(event)
		{
			if(settings.timeout != null)
			{
				clearTimeout(settings.timeout);
			}

			settings.timeout = setTimeout(function()
			{
				resizeFacebook();
			}, 600);
		});

		$('#fb-root').on('facebook:init', function(){ resizeFacebook(); });


	}); 


};



})(jQuery);

